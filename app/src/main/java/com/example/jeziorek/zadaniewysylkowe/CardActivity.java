package com.example.jeziorek.zadaniewysylkowe;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jeziorek.zadaniewysylkowe.data.BaseData;
import com.example.jeziorek.zadaniewysylkowe.data.Card;
import com.example.jeziorek.zadaniewysylkowe.data.Cards;
import com.example.jeziorek.zadaniewysylkowe.data.Deck;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class CardActivity extends AppCompatActivity {
    String countDeck;
    RestApiEnd restApi;
    List<Card> myCards;//wyciagniete 5 kart
    TextView reaming;
    ImageView card1,card2,card3,card4,card5;

    final String[] figureCards = {"ACE","2","3","4","5","6","7","8","9","10","J","Q","K"};// kolejnosc jest wazna
    final String[] colorCards = {"HEARTS", "CLUBS","SPADES","DIAMONDS"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        Bundle extras = getIntent().getExtras();
        countDeck = extras.getString(BaseData.COUNTDECK); //pobranie liczby talii
        restApi = BaseData.retrofit.create(RestApiEnd.class);
        myCards = new ArrayList<Card>();
        reaming = (TextView) findViewById(R.id.TV_reaming);
        card1 = (ImageView) findViewById(R.id.IV_card1);
        card2 = (ImageView) findViewById(R.id.IV_card2);
        card3 = (ImageView) findViewById(R.id.IV_card3);
        card4 = (ImageView) findViewById(R.id.IV_card4);
        card5 = (ImageView) findViewById(R.id.IV_card5);
        setNOne();

        //uruchomienie w osobnym watku pobranie nowej talii
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                getNewDeck(countDeck);
                if(BaseData.shuffledDeck == "false") shuffle(BaseData.idDeck);
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        refresh();
    }
    public void refresh(){
        reaming.setText("Pozostala ilosc kart: "+BaseData.remainingDeck);
    }
    public void setNOne(){
        card1.setImageResource(R.mipmap.none);
        card2.setImageResource(R.mipmap.none);
        card3.setImageResource(R.mipmap.none);
        card4.setImageResource(R.mipmap.none);
        card5.setImageResource(R.mipmap.none);
    }
    //wywolanie metody przyciskiem wybierz karty
    public void getCardfromToHand(View view){
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                    getCard("5",BaseData.idDeck);
               }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        refresh();
        if(myCards.size()>0) {
            if(checkComposition().size()>0) {
                combination(checkComposition());
            }
        }
    }

    //sprawdzanie wszystkich ulozen i dodawanie występujących do listy
    public List<String> checkComposition(){
        List<String> list = new ArrayList<>();
        if(color()!= "") list.add(color());
        if(steps()!="") list.add(steps());
        if(twins()!="") list.add(twins());
        if(figure()!="") list.add(figure());

        return list;
    }

    //wyszukuje figury w (5) pobranych kartach
    public boolean searchFigure(String figure){
        for (Card choose : myCards) {
            if(choose.getValue().equals(figure)) return true;
        }
        return false;
    }

    //karty w tym samym kolorze
    public String color(){
        for (String color : colorCards) {
            int i = 0;
            for (Card choose : myCards) {
                if (choose.getSuit().equals(color)) {
                    i++;
                }
            }
            if (i >= 3) {
                return "kolor: "+color;
            }
        }
        return "";
    }

    //szukanie 3 kart w kolejnosci
    public String steps() {
        for (Card choose : myCards) {
            for (int i = 0; i < figureCards.length; i++) {
                if (choose.getValue().equals(figureCards[i])) {
                    if (i < 0 && i + 1 < figureCards.length) {
                        if (searchFigure(figureCards[i - 1]) && searchFigure(figureCards[i + 1])) return "schodki: "+figureCards[i-1]+","+figureCards[i]+","+figureCards[i+1];
                    } else if(i==0){
                        if (searchFigure(figureCards[i + 1]) && searchFigure(figureCards[i + 2])) return "schodki: "+figureCards[i]+","+figureCards[i+1]+","+figureCards[i+2];
                    }else if(i+1==figureCards.length){
                        if (searchFigure(figureCards[i - 1]) && searchFigure(figureCards[i - 2])) return "schodki: "+figureCards[i-2]+","+figureCards[i-1]+","+figureCards[i];
                    }
                }
            }
        }
        return "";
    }

    //3 takiesame wartosci
    public String twins(){
        for (Card figure : myCards) {
            int i = 0;
            for (Card choose : myCards) {
                if (choose.getValue().equals(figure)) {
                    i++;
                }
            }
            if (i >= 3) {
                return "blizniaki: "+figure.getValue();
            }
        }
        return "";
    }

    //sprawdzam wystepowanie figury
    public String figure(){
        String figure = "";
        int i = 0;
        if(searchFigure(figureCards[0])) {
            figure = figure+" "+figureCards[0];
            i++;
        }
        if(searchFigure(figureCards[10])){
            figure = figure+" "+figureCards[10];
            i++;
        }
        if(searchFigure(figureCards[11])){
            figure = figure+" "+figureCards[11];
            i++;
        }
        if(searchFigure(figureCards[12])){
            figure = figure+" "+figureCards[12];
            i++;
        }
        if(figure!="" && i>=3)return "figury: "+figure;
        return "";
    }

    public void getNewDeck(String count){
        Call<Deck> call = restApi.getDeck(count);
        try{
            Response<Deck> response = call.execute();
            BaseData.idDeck = response.body().getDeckId();
            BaseData.remainingDeck = response.body().getRemaining();
            BaseData.shuffledDeck = response.body().getShuffled();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //pobiera określoną ilość kart
    public void getCard(String numberCards, String idDeck){
        if(Integer.parseInt(BaseData.remainingDeck)<5){
            shuffle(BaseData.idDeck);
        }else {
            myCards.clear();
            Call<Cards> call = restApi.getCard(idDeck, numberCards);
            try {
                Response<Cards> response = call.execute();
                BaseData.remainingDeck = response.body().getRemaining();
                for (int i = 0; i < response.body().getCards().size(); i++) {
                    myCards.add(response.body().getCards().get(i));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            new DownloadImageTask(card1).execute(myCards.get(0).getImage());
            new DownloadImageTask(card2).execute(myCards.get(1).getImage());
            new DownloadImageTask(card3).execute(myCards.get(2).getImage());
            new DownloadImageTask(card4).execute(myCards.get(3).getImage());
            new DownloadImageTask(card5).execute(myCards.get(4).getImage());
        }
    }

    //tasowanie wybranej talii
    public void shuffle(String idDeck){
        Call<Deck> call = restApi.shuffleDeck(idDeck);
        try{
            Response<Deck> response = call.execute();
            BaseData.remainingDeck = response.body().getRemaining();
            BaseData.shuffledDeck = response.body().getShuffled();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void combination(List<String> com) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("znalezione kombinacje to:");
        String tmp = "";
        for (String combination : com) {
            tmp = tmp+" "+combination+"\n";
        }
        builder1.setMessage(tmp);
        builder1.setCancelable(true);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}
