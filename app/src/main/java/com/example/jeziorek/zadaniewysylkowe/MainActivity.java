package com.example.jeziorek.zadaniewysylkowe;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jeziorek.zadaniewysylkowe.data.BaseData;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    Spinner countDeck;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        countDeck = (Spinner) findViewById(R.id.SP_deckCount) ;
        BaseData.retrofit = new Retrofit.Builder()
                .baseUrl(BaseData.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public boolean checkInternetConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void start(View view){

        if(checkInternetConnection()) {
            Intent i = new Intent(this.getApplicationContext(), CardActivity.class);
            i.putExtra(BaseData.COUNTDECK,countDeck.getSelectedItem().toString());
            startActivity(i);
        }else{
            Toast.makeText(this.getApplicationContext(),"Wymagane połączenie z internetem",Toast.LENGTH_LONG).show();
        }
    }
}