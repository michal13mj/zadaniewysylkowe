package com.example.jeziorek.zadaniewysylkowe;

import com.example.jeziorek.zadaniewysylkowe.data.Cards;
import com.example.jeziorek.zadaniewysylkowe.data.Deck;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface RestApiEnd {

    @GET("new/shuffle/")
    Call<Deck> getDeck(@Query("deck_count") String deck_count);

    @GET("{deck_id}/shuffle")
    Call<Deck> shuffleDeck(@Path("deck_id") String deck_id);

    @GET("{deck_id}/draw/")
    Call<Cards> getCard(@Path("deck_id") String deck_id, @Query("count") String count);

}
