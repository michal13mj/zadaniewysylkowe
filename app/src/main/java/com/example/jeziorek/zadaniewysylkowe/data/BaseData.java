package com.example.jeziorek.zadaniewysylkowe.data;

import retrofit2.Retrofit;

public class BaseData {
    public static final String BASE_URL = "https://deckofcardsapi.com/api/deck/";
    public static Retrofit retrofit;
    public static String idDeck;
    public static String shuffledDeck;
    public static String remainingDeck;



    //KEY
    public final static String COUNTDECK = "countDeck";
}
