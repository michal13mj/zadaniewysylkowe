package com.example.jeziorek.zadaniewysylkowe.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cards {
    @SerializedName("cards")
    @Expose
    private List<Card> cards;

    @SerializedName("deck_id")
    @Expose
    private String deckId;

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("remaining")
    @Expose
    private String remaining;

    public List<Card> getCards() {
        return cards;
    }

    public String getRemaining() {
        return remaining;
    }

}
