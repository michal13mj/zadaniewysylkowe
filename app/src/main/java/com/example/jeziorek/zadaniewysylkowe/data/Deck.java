package com.example.jeziorek.zadaniewysylkowe.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Deck {

    @SerializedName("deck_id")
    @Expose
    private String deckId;

    @SerializedName("success")
    @Expose
    private String success;

    @SerializedName("shuffled")
    @Expose
    private String shuffled;

    @SerializedName("remaining")
    @Expose
    private String remaining;

    public String getDeckId() {
        return deckId;
    }
    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }
    public String getSuccess() {
        return success;
    }
    public String getShuffled() {
        return shuffled;
    }
    public String getRemaining() {
        return remaining;
    }
    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }
    @Override
    public String toString() {
        return success+", "+deckId+", "+shuffled+", "+remaining;
    }
}
